# [Date Range Model]

The DateRange model is an entity which can be used on a back-end of dateRange picker component.
It contains start and end date. The start date must be less than an end date.

 ## Quick start

 * To create an instance of the model pass startDate and endDate as a string values (format is dd/MM/yyyy) to the valueOf() method.
    DateRangeModel.valueOf("01/10/2012", "15/10/2012");

 * The model is considered to be valid if it has both start and end date
    use dateRangeModel.isValid() to check it

 * This model does not deal with time part of the date, so 01/10/12 18:00 will be stored as 01/10/12.
    DateRangeModel dateRangeModel = DateRangeModel.valueOf("01/10/2012 18:00", "15/10/2012 09:00");
    dateRangeModel.getStartDate() -> "01/10/2012"
    dateRangeModel.getEndDate() -> "15/10/2012"

 * This is an immutable class, so calls to valueOf method will always return a new object.

 ## Features

 * This model can calculate days difference between start ans end date:
    ("01/10/2012", "15/10/2012") -> 15

 * This model can calculate months difference between start and end date. If the value if less than a month it will return a proportion.
    ("01/09/2012", "15/10/2012") - > 1.5 months
    ("01/10/2012", "15/10/2012") - > 0.5 months

    NOTE: This method assumes that a month has 30 days on average. So the result will be approximate!

 * This model can determine whether provided date is in the date range (i.e. is between start and end date)

 ## Improvements

 * Made a date format pattern configurable value. Not it's a constant of 'dd/MM/yyyy'.
 * Check if model is valid before calculating difference between dates.

 ## API

 * This model can be used with REST API.
    For instance in order to create a new model the URL could look like:
      http://[company-url]/[service]/createDateRange/[startDate]/[endDate]

 * To retrieve either start or end date the following url could be used:
      http://[company-url]/[service]/getStartDate

      The result can be in JSON format:
      {
        "date": "01/10/2012"
      }

 etc

 ## Contributing

 * Slava Ustovytski