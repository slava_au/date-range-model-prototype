import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Unit test for simple DateRangeModel.
 */
public class DateRangeModelTest {

    @Test
    public void valueOf_WillCreateNewRangeObject_WhenProvidedDatesAreValid() {
        // prepare
        final String startDate = "01/10/2012";
        final String endDate = "15/10/2012";

        // execute
        DateRangeModel dateRangeModel = DateRangeModel.valueOf(startDate, endDate);

        // assert
        assertThat(dateRangeModel, notNullValue());
        assertThat(dateRangeModel.getStartDate(), equalTo(startDate));
        assertThat(dateRangeModel.getEndDate(), equalTo(endDate));
    }

    @Test
    public void valueOf_WillCreateNewRangeWithoutTimeComponent_WhenProvidedDatesAreProvidedWithTime() {
        // prepare
        final String startDateWithTime = "01/10/2012 18:00";
        final String endDateWithTime = "15/10/2012 09:00";


        // execute
        DateRangeModel dateRangeModel = DateRangeModel.valueOf(startDateWithTime, endDateWithTime);

        // assert
        assertThat(dateRangeModel, notNullValue());
        final String expectedStartDate = "01/10/2012";
        assertThat(dateRangeModel.getStartDate(), equalTo(expectedStartDate));
        final String expectedEndDate = "15/10/2012";
        assertThat(dateRangeModel.getEndDate(), equalTo(expectedEndDate));
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueOf_WillThrowException_WhenProvidedEndDateLessThatStartDate() {
        // prepare
        final String startDate = "15/10/2012";
        final String endDate = "01/10/2012";

        // execute
        DateRangeModel.valueOf(startDate, endDate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueOf_WillThrowException_WhenProvidedStartDateIsInDifferentFormat() {
        // prepare
        final String startDate = "01-10-2012";
        final String endDate = "15/10/2012";

        // execute
        DateRangeModel.valueOf(startDate, endDate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueOf_WillThrowException_WhenProvidedEndDateIsInDifferentFormat() {
        // prepare
        final String startDate = "01/10/2012";
        final String endDate = "15-10-2012";

        // execute
        DateRangeModel.valueOf(startDate, endDate);
    }

    @Test
    public void isValid_WillReturnFalse_WhenEitherOneOfDatesAreEmpty(){
        // prepare
        final String startDate = "01/10/2012";
        final String endDate = null;

        // execute
        DateRangeModel dateRangeModel = DateRangeModel.valueOf(startDate, endDate);

        // assert
        assertThat(dateRangeModel, notNullValue());
        assertThat(dateRangeModel.isValid(), is(false));
    }

    @Test
    public void isValid_WillReturnTrue_WhenAllDatesAreSet() {
        // prepare
        final String startDate = "01/10/2012";
        final String endDate = "15/10/2012";

        // execute
        DateRangeModel dateRangeModel = DateRangeModel.valueOf(startDate, endDate);

        // assert
        assertThat(dateRangeModel, notNullValue());
        assertThat(dateRangeModel.isValid(), is(true));
    }

    @Test
    public void getDaysBetween_WillReturnNumberOfDaysBetweenTwoDates_WhenDatesAreInSameMonth() {
        // prepare
        final String startDate = "01/10/2012";
        final String endDate = "15/10/2012";

        // execute
        DateRangeModel dateRangeModel = DateRangeModel.valueOf(startDate, endDate);

        // assert
        assertThat(dateRangeModel.getDaysBetween(), is(15));
    }

    @Test
    public void getMonthsBetween_WillReturnMonthBetweenStartAndEndDate() {
        // prepare
        String startDate = "01/09/2012";
        String endDate = "30/09/2012";

        // execute
        DateRangeModel dateRangeModel = DateRangeModel.valueOf(startDate, endDate);

        // assert
        assertThat(dateRangeModel.getMonthsBetween(), is(1.0));

        // prepare
        startDate = "01/09/2012";
        endDate = "15/10/2012";

        // execute
        dateRangeModel = DateRangeModel.valueOf(startDate, endDate);

        // assert
        assertThat(dateRangeModel.getMonthsBetween(), is(1.5));

        // prepare
        startDate = "01/10/2012";
        endDate = "15/10/2012";

        // execute
        dateRangeModel = DateRangeModel.valueOf(startDate, endDate);

        // assert
        assertThat(dateRangeModel.getMonthsBetween(), is(0.5));
    }

    @Test
    public void isBetweenDateRange_WillReturnTrue_WhenProvidedDateIsBetweenStartAndEndDateOfRange() {
        // prepare
        final String startDate = "01/09/2012";
        final String endDate = "15/10/2012";

        // execute
        DateRangeModel dateRangeModel = DateRangeModel.valueOf(startDate, endDate);

        // assert
        assertThat(dateRangeModel.isBetweenDateRange("01/10/2012"), is(true));
    }

    @Test
    public void isBetweenDateRange_WillReturnFalse_WhenProvidedDateIsNotInTheRange() {
        // prepare
        final String startDate = "01/09/2012";
        final String endDate = "15/10/2012";

        // execute
        DateRangeModel dateRangeModel = DateRangeModel.valueOf(startDate, endDate);

        // assert
        assertThat(dateRangeModel.isBetweenDateRange("01/11/2012"), is(false));
    }
}
