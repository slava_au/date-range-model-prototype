import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * The DateRange model representation.
 *
 * The dateRange is an entity which contains start and end date. The start date must be less than an end date.
 *
 * To create an instance of the model pass startDate and endDate as a string values (format dd/MM/yyyy) to the valueOf() method.
 * This is an immutable class, so calls to valueOf method will always return a new object.
 *
 * This model does not deal with time part, so 01/10/12 18:00 will be stored as 01/10/12.
 *
 * Ideally the date format pattern should be a configurable value. Right now it's a constant value of 'dd/MM/yyyy'.
 *
 * @author Slava Ustovytski.
 */
public class DateRangeModel {

    private static final String DATE_FORMAT_PATTERN = "dd/MM/yyyy";

    private final Date startDate;
    private final Date endDate;

    /**
     * Private constructor to prevent creation of instances outside of this class.
     *
     * @see #valueOf(String, String) for more info.
     */
    private DateRangeModel(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * A factory method used to create a new instances of a DateRange model.
     * This is an immutable class so all the instances returned by this method will be a separate (new) objects.
     *
     * @param startDate string representation of the startDate in a format of #DATE_FORMAT_PATTERN.
     * @param endDate   string representation of the endSate in a format of #DATE_FORMAT_PATTERN.
     * @throws IllegalArgumentException when endDate is less than startEnd.
     * @throws IllegalArgumentException when provided date string does not match the #DATE_FORMAT_PATTERN.
     *
     * @return a new DateRageModel instance.
     */
    public static DateRangeModel valueOf(String startDate, String endDate) {

        Date startDateValue = parseDate(startDate);

        Date endDateValue = parseDate(endDate);

        if (endDate != null && startDate != null
                && endDateValue.before(startDateValue)) {
            throw new IllegalArgumentException("Start date must be less than end date");
        }

        return new DateRangeModel(startDateValue, endDateValue);
    }

    /**
     * Returns false if either of start and end date are not set, true otherwise.
     */
    public boolean isValid() {
        return startDate != null && endDate != null;
    }

    /**
     * Calculate number of days between start and end date (inclusive).
     *
     * <p>
     *     For instance: (01/10/2012, 15/10/2012) -> 15 days.
     * </p>
     *
     * @return number of days.
     */
    public int getDaysBetween() {
        Calendar startDateCal = GregorianCalendar.getInstance();
        startDateCal.setTime(startDate);

        Calendar endDateCal = GregorianCalendar.getInstance();
        endDateCal.setTime(endDate);

        return (endDateCal.get(Calendar.DAY_OF_YEAR) + 1) - startDateCal.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * Calculate number of months between start and end date (inclusive).
     * If it's less than a month then return a proportion of the months.
     *
     * NOTE: This method assumes that average months contains 30 days so results are approximate.
     *
     * <p>
     *     For instance: (01/09/2012, 30/09/2012) -> 1 month
     *                   (01/09/2012, 15/10/2012) -> 1.5 months
     * </p>
     *
     * @return number of months between dates.
     */
    public double getMonthsBetween() {
        // approximate 30 days a month.
        double daysBetween = getDaysBetween();
        return daysBetween / 30;
    }

    /**
     * Returns true if provided date is between start and end date of the range, false otherwise.
     *
     * @param date string representation of the date in a format of #DATE_FORMAT_PATTERN.
     *
     * @return boolean value.
     */
    public boolean isBetweenDateRange(String date) {
        Date providedDate = parseDate(date);
        return startDate.before(providedDate) && providedDate.before(endDate);
    }

    /**
     * Returns start date of the range in String format.
     */
    public String getStartDate() {
        return new SimpleDateFormat(DATE_FORMAT_PATTERN).format(startDate);
    }

    /**
     * Returns end date of the range in String format.
     */
    public String getEndDate() {
        return new SimpleDateFormat(DATE_FORMAT_PATTERN).format(endDate);
    }

    private static Date parseDate(String dateStringValue) {
        Date startDateValue = null;
        if (dateStringValue != null && dateStringValue.length() > 0) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                startDateValue = simpleDateFormat.parse(dateStringValue);
            } catch (ParseException e) {
                throw new IllegalArgumentException("Provided date should be of the following format: " + DATE_FORMAT_PATTERN, e);
            }
        }
        return startDateValue;
    }

    @Override
    public boolean equals(Object o) {
        // In production code use: EqualsBuilder.reflectionEquals(this, obj, false);
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DateRangeModel that = (DateRangeModel) o;

        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        // In production code use: HashCodeBuilder.reflectionHashCode(this, false);
        int result = startDate != null ? startDate.hashCode() : 0;
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        return result;
    }
}
